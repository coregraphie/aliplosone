import sys, warnings
import numpy as np
import pandas as pd
import networkx as nx
from collections import defaultdict
from scipy.stats import binom


def nc_backbone_prximation(g):
    n = sum(nx.get_edge_attributes(g, name='weight').values())

    for i, j, w in g.edges(data='weight'):
        ni= g.degree(i, weight='weight')
        nj= g.degree(j, weight='weight')
        pij = ((ni * nj) / n) * (1 / n)
        g[i][j]['nc_score'] = binom.cdf(w, n, pij) 
        
    return g


def nc_backbone(g, approximation=False):
   n = sum(nx.get_edge_attributes(g, name='weight').values())

   for i, j, w in g.edges(data='weight'):
      ni= g.degree(i, weight='weight')
      nj= g.degree(j, weight='weight')
      mean_prior_probability = ((ni * nj) / n) * (1 / n)
      kappa = n / (ni * nj)
      if approximation:
         g[i][j]['nc_score'] = binom.cdf(w, n, mean_prior_probability) 
      else:
         score = ((kappa * w) - 1) / ((kappa * w) + 1)
         var_prior_probability = (1 / (n ** 2)) * (ni * nj * (n - ni) * (n - nj)) / ((n ** 2) * ((n - 1)))
         alpha_prior = (((mean_prior_probability ** 2) / var_prior_probability) * (1 - mean_prior_probability)) - mean_prior_probability
         beta_prior = (mean_prior_probability / var_prior_probability) * (1 - (mean_prior_probability ** 2)) - (1 - mean_prior_probability)

         alpha_post = alpha_prior + w
         beta_post = n - w + beta_prior
         expected_pij = alpha_post / (alpha_post + beta_post)
         variance_nij = expected_pij * (1 - expected_pij) * n
         d = (1.0 / (ni * nj)) - (n * ((ni + nj) / ((ni * nj) ** 2)))
         variance_cij = variance_nij * (((2 * (kappa + (w * d))) / (((kappa * w) + 1) ** 2)) ** 2) 
         sdev_cij = variance_cij ** .5
         g[i][j]['nc_sdev'] = sdev_cij
         g[i][j]['nc_score'] = score
      
   return g

def noise_corrected(table, undirected = False, return_self_loops = False, calculate_p_value = False):

   # table["kappa"] = table["n.."] / (table["ni."] * table["n.j"])
   # table["score"] = ((table["kappa"] * table["nij"]) - 1) / ((table["kappa"] * table["nij"]) + 1)
   # table["var_prior_probability"] = (1 / (table["n.."] ** 2)) * (table["ni."] * table["n.j"] * (table["n.."] - table["ni."]) * (table["n.."] - table["n.j"])) / ((table["n.."] ** 2) * ((table["n.."] - 1)))
   # table["alpha_prior"] = (((table["mean_prior_probability"] ** 2) / table["var_prior_probability"]) * (1 - table["mean_prior_probability"])) - table["mean_prior_probability"]
   # table["beta_prior"] = (table["mean_prior_probability"] / table["var_prior_probability"]) * (1 - (table["mean_prior_probability"] ** 2)) - (1 - table["mean_prior_probability"])
   table["alpha_post"] = table["alpha_prior"] + table["nij"]
   table["beta_post"] = table["n.."] - table["nij"] + table["beta_prior"]
   table["expected_pij"] = table["alpha_post"] / (table["alpha_post"] + table["beta_post"])
   table["variance_nij"] = table["expected_pij"] * (1 - table["expected_pij"]) * table["n.."]
   table["d"] = (1.0 / (table["ni."] * table["n.j"])) - (table["n.."] * ((table["ni."] + table["n.j"]) / ((table["ni."] * table["n.j"]) ** 2)))
   table["variance_cij"] = table["variance_nij"] * (((2 * (table["kappa"] + (table["nij"] * table["d"]))) / (((table["kappa"] * table["nij"]) + 1) ** 2)) ** 2) 
   table["sdev_cij"] = table["variance_cij"] ** .5
   if not return_self_loops:
      table = table[table["src"] != table["trg"]]
   if undirected:
      table = table[table["src"] <= table["trg"]]
   return table[["src", "trg", "nij", "score", "sdev_cij"]]